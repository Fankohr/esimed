package fr.esimed.tp

fun main(args: Array<String>) {
    println(levenshteinDistance("NICHE","CHIENS")); //Resultat attendu : 5
    println(levenshteinDistance("avion","aviron")); //Resultat attendu : 1
    println(levenshteinDistance("avion","avion")); //Resultat attendu : 0
    println(levenshteinDistance("Arnaud","Julien")); //Resultat attendu : 0
    println(levenshteinDistance("Julie","Julien")); //Resultat attendu : 0
}

fun levenshteinDistance(a: String, b: String) : Int {
    var d: ArrayList<ArrayList<Int>>;
    var cout: Int;

    d = ArrayList(a.length + 1);

    for(i in 0 until a.length + 1) {
        d.add(i, ArrayList(b.length + 1))
        d[i].add(0, i);
        for (j in 1 until b.length + 1) {
            d[i].add(j, 0);
        }
    }

    for (j in 0 until b.length + 1) {
        d[0].add(j, j);
    }

    for (i in 1 until a.length+1) {
        for (j in 1 until b.length+1) {
            if(a.substring(i-1, i) == b.substring(j-1, j)) {
                cout = 0;
            } else {
                cout = 1;
            }

            d[i][j] = Math.min(Math.min(
                d[i-1][j] + 1,
                d[i][j-1] + 1),
                d[i-1][j-1] + cout
            )
        }
    }
    return d[a.length][b.length];
}